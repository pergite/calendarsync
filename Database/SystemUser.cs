﻿using System;
using System.Linq;
using Starcounter;

namespace CalendarSync.Database
{
    [Database]
    public class SystemUser
    {
        public string UserName { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public DateTime Created { get; set; }

        public bool AllowAdminSettings { get; set; }
        public bool AllowSyncCalendar { get; set; }

        public static SystemUser Current
        {
            get
            {
                var su = Db.SQL<SystemUser>("SELECT su FROM SystemUser su").FirstOrDefault();

#if RUN_WITHOUT_MAPPING

                if (null == su)
                {
                    Db.Transact(() =>
                    {
                        su = new SystemUser()
                        {
                            UserName = "admin",
                            Name = "Admini Adminsson",
                            Email = "jo@pergite.com",
                            Created = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc),
                            AllowAdminSettings = true,
                            AllowSyncCalendar = true
                        };
                    });
                }
#endif
                return su;
            }
        }
    }
}
