﻿using System;
using System.Collections.Generic;
using Starcounter;

namespace CalendarSync.Database
{
    public enum SyncedCalendarTypeEnum
    {
        Calendar = 1,
        Resource = 2
    }

    [Database]
    public class SyncedCalendar
    {
        public string Provider { get; set; }
        public string ProviderId { get; set; }
        public string CalendarId { get; set; }
        public string Name { get; set; }
        public SyncedCalendarTypeEnum Type { get; set; }
        public DateTime? LastSync { get; set; }
        public OAUser Owner { get; set; }

        public IEnumerable<SyncedEvent> Events => Db.SQL<SyncedEvent>("SELECT se FROM CalendarSync.Database.SyncedEvent se WHERE se.Calendar=?", this);

        /* INTERNAL -SHOULT NOT BE MAPPED */
        public string SyncToken { get; set; }
        public string WatchId { get; set; }
        public string WatchRemoteId { get; set; }
        public DateTime WatchExpires { get; set; }
        /* INTERNAL - END */
    }
}
