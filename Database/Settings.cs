﻿using System;
using Starcounter;

namespace CalendarSync.Database
{
    [Database]
    public class Settings
    {
        public bool GoogleEnabled { get; set; }
        public string GoogleClientID { get; set; }
        public string GoogleClientSecret { get; set; }

        public bool MicrosoftEnabled { get; set; }
        public string MicrosoftClientID { get; set; }
        public string MicrosoftClientSecret { get; set; }
    }
}
