﻿using System;
using System.Collections.Generic;
using Starcounter;

namespace CalendarSync.Database
{
    public enum StatusEnum
    {
        Declined = -1,
        Tentative = 0,
        Confirmed = 1
    }

    [Database]
    public class Contact
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }

    [Database]
    public class SyncedEventParticipant
    {
        public SyncedEvent Event { get; set; }
        public Contact Contact { get; set; }
        public StatusEnum Status { get; set; }
    }

    [Database]
    public class SyncedEvent
    {
        public SyncedCalendar Calendar;

        public DateTime BeginTime { get; set; }
        public DateTime EndTime { get; set; }
        public bool FullDay { get; set; }

        public StatusEnum Status { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }

        public Contact Organizer { get; set; }

        public IEnumerable<SyncedEventParticipant> Participants => Db.SQL<SyncedEventParticipant>("SELECT sep FROM CalendarSync.Database.SyncedEventParticipant sep WHERE sep.SyncedEvent=?", this);

        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }

        public bool IsDeleted { get; set; }

        /* INTERNAL - SHOULD NOT BE MAPPED */
        public bool RemoteOrigin { get; set; }
        public string RemoteId { get; set; }
        public string iCalUID { get; set; }
        /* INTERNAL - END */
    }
}
