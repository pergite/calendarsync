﻿using System;
using System.Collections.Generic;
using System.Timers;
using Starcounter;

using OAuth2.Core.Helpers;

using CalendarSync.Models;
using CalendarSync.Database;
using CalendarSync.Helpers;
using CalendarSync.Services;
using CalendarSync.Notifications;

namespace CalendarSync.Tasks
{
    public class Periodic
    {
        private const int EVERY_10_MINUTES = 10 * 60 * 1000;
        private static Timer timer;

        public static void Tick()
        {
            Scheduling.RunTask(() =>
            {
                // see if any watches are about to expire
                foreach (var sc in Db.SQL<SyncedCalendar>("SELECT sc FROM CalendarSync.Database.SyncedCalendar sc WHERE sc.WatchId IS NOT NULL"))
                {
                    if (DateTime.UtcNow >= sc.WatchExpires.AddHours(1))
                    {
                        NotificationClient.RefreshWatch(sc);
                    }
                }
            });
        }

        public static void OnTimer(object sender, ElapsedEventArgs eea)
        {
            Tick();
        }

        public static void Start()
        {
            if (null != timer)
                return;

            timer = new Timer(EVERY_10_MINUTES)
            {
                AutoReset = true
            };

            timer.Elapsed += OnTimer;
            timer.Start();
        }

        public static void Stop()
        {
            if (null == timer)
                return;

            timer.Stop();
            timer = null;
        }

    }
}
