﻿using System;
using System.Web;
using System.Linq;
using Starcounter;

using OAuth2.Core.Handlers;
using OAuth2.Core.Helpers;

using CalendarSync.Handlers;
using CalendarSync.Database;
using CalendarSync.Models;
using CalendarSync.ViewModels;
using CalendarSync.Helpers;
using CalendarSync.Tasks;
using CalendarSync.Notifications;
using CalendarSync.Services;



namespace CalendarSync
{
    class Program
    {
        static void Main()
        {
            Application.Current.Use(new HtmlFromJsonProvider());
            Application.Current.Use(new PartialToStandaloneHtmlProvider());

            // OAuth2Flow
            // handles all OAuth2 signin logic with necessary communications with the oauth2, provider
            OAuth2Flow.Register(new Uri("/calendarsync/", UriKind.Relative), SettingsManager.Instance, UserManager.Instance);

            // Commithooks
            Hooks.Register();

            // request handlers
            MainHandlers.Register();

            // run a full sync for all calendars at startup
            AllSyncedCalendars.FullSync();

            // start timer to refresh outdated watchnotifications
            Periodic.Start();

            // start the notification client that connects to the eventdispatch service
            NotificationClient.Start();
        }
    }
}
