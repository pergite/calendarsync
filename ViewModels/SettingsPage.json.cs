using System;
using System.Linq;
using Starcounter;


using CalendarSync.Database;
using CalendarSync.Models;
using CalendarSync.Helpers;
using CalendarSync.Notifications;
using CalendarSync.Services;

using OAuth2.Core.Services;

namespace CalendarSync.ViewModels
{

    partial class SettingsPage : Json
    {
        [SettingsPage_json.SyncedCalendars]
        partial class SyncedCalendarItem : Json
        {
            void Handle(Input.Sync action)
            {
            }

            void Handle(Input.Remove action)
            {
                var sc = Db.SQL<SyncedCalendar>("SELECT sc FROM CalendarSync.Database.SyncedCalendar sc WHERE sc.Provider=? AND sc.CalendarId=?", action.App.Provider, action.App.CalendarId).FirstOrDefault();

                // Stop notifications
                NotificationClient.RemoveWatch(sc);

                // We dont want our hooks to run as they would remove the events from the Google Calendar too...
                DbX.TransactIgnoreHooks(() =>
                {
                    Db.SQL("DELETE FROM CalendarSync.Database.SyncedEvent WHERE Calendar=?", sc);
                    sc.Delete();
                });

                Session.Ensure().CalculatePatchAndPushOnWebSocket(); // this supposed to work?
            }

            void Handle(Input.CreateTestEvent action)
            {
                var sc = Db.SQL<SyncedCalendar>("SELECT sc FROM CalendarSync.Database.SyncedCalendar sc WHERE sc.Provider=? AND sc.CalendarId=?", action.App.Provider, action.App.CalendarId).FirstOrDefault();

                Db.Transact(() =>
                {
                    if(String.IsNullOrEmpty(action.App.CreatedEventId))
                    {
                        var se = new SyncedEvent()
                        {
                            Calendar = sc,
                            BeginTime = DateTime.UtcNow.AddHours(1),
                            EndTime = DateTime.UtcNow.AddHours(3),
                            Name = "Test created  Event",
                            Description = "This is a test event created from the CalendarSync app",
                            Location = "Cyberspace"
                        };

                        action.App.CreatedEventId = se.GetObjectID();
                    }
                    else
                    {
                        var se = Db.SQL<SyncedEvent>("SELECT se FROM CalendarSync.Database.SyncedEvent se WHERE se.ObjectID=?", action.App.CreatedEventId).FirstOrDefault();

                        se.BeginTime = se.BeginTime.AddHours(1);
                        se.EndTime = se.EndTime.AddHours(1);
                    }
                });
            }
        }

        protected override void OnData()
        {
            base.OnData();

            //page.SyncedCalendars.Data = SyncedCalendar.List(SystemUser.GetCurrentSystemUser());

            //
            // As "typed json" doesn't seem to support nullable datatypes I had to workaround by populating
            // manually rather than using the .Data binding...perhaps theres some other way, but I
            // wasnt able to find out how....
            SystemUser user = SystemUser.Current;

            foreach (var esc in SCHelper.SyncedCalendars(user))
            {
                var sc = this.SyncedCalendars.Add();

                sc.Provider = esc.Provider;
                sc.Name = esc.Name;
                sc.Type = esc.Type.ToString();
                sc.CalendarId = esc.CalendarId;
                sc.LastSync = esc.LastSync.HasValue ? esc.LastSync.Value.ISO8601() : null;
            }

            // TODO: load async?
            ServiceBase google = user.GetOAUser("Google").GetService();

            // list users own calendars
            var gcList = google.Get<GCCalendarList>("https://www.googleapis.com/calendar/v3/users/me/calendarList");

            foreach (var gc in gcList.items.Where(gc => gc.accessRole == "owner"))
            {
                var ac = this.AvailableCalendars.Add();

                ac.Id = gc.id;
                ac.Type = "Calendar";
                ac.Name = gc.summary;
            }

            // list users owned calendar resources (if any, will throw a RestException if user doesnt have any)
            try
            {
                var rcl = google.Get<GCResourceCalendarList>("https://www.googleapis.com/admin/directory/v1/customer/my_customer/resources/calendars");

                foreach(var grc in rcl.items)
                {
                    for(var i=0; i<this.AvailableCalendars.Count; i++)
                    {
                        if(this.AvailableCalendars[i].Id==grc.resourceEmail)
                        {
                            this.AvailableCalendars.RemoveAt(i);
                            break;
                        }
                    }

                    var ac = this.AvailableCalendars.Add();

                    ac.Id = grc.resourceEmail;
                    ac.Type = "Resource";
                    ac.Name = grc.resourceName;
                }
            }
            catch(RestException rex)
            {
                Console.WriteLine("resources/calendars failed {0} {1}", rex.StatusCode, rex.ResponseContent);
            }
        }

        void Handle(Input.Add action)
        {
            SyncedCalendar sc = null;

            Db.Transact(() =>
            {
                var user = SystemUser.Current;
                var googleUser = user.GetOAUser("Google");
                var cal = AvailableCalendars.Where(ac => ac.Id == SelectedCalendarId).First();

                sc = new SyncedCalendar()
                {
                    Provider = "Google",
                    ProviderId = googleUser.ProviderId,
                    CalendarId = SelectedCalendarId,
                    Type = (SyncedCalendarTypeEnum)Enum.Parse(typeof(SyncedCalendarTypeEnum), cal.Type, true),
                    Name = cal.Name,
                    Owner = googleUser,
                    LastSync = null
                };

                var p = SyncedCalendars.Add();
                p.Name = sc.Name;
                p.Provider = sc.Provider;
                p.CalendarId = sc.CalendarId;
                p.Type = sc.Type.ToString();
                p.LastSync = null;

            });

            SyncBase.TryFullSync(sc);
            NotificationClient.AddWatch(sc);
        }

        void Handle(Input.Reset action)
        {
            NotificationClient.Reset();
        }
    }
}
