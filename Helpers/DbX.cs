﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starcounter;

namespace CalendarSync.Helpers
{
    public class DbX
    {
        public static void TransactIgnoreHooks(Action action, int maxRetries = 100)
        {
            Db.Transact(action, new Db.Advanced.TransactOptions
            {
                maxRetries = maxRetries,
                applyHooks = false
            });
        }
    }
}
