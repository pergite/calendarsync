﻿using CalendarSync.Database;
using CalendarSync.Models;
using Starcounter;
using System.Collections.Concurrent;

namespace CalendarSync.Helpers
{
    public class DeleteQueue
    {
        static DeleteQueue _instance = new DeleteQueue();
        ConcurrentDictionary<ulong, DeletedSyncedEvent> _queue = new ConcurrentDictionary<ulong, DeletedSyncedEvent>();
        
        public void Set(SyncedEvent se)
        {
            _queue[se.GetObjectNo()] = new DeletedSyncedEvent()
            {
                Calendar = se.Calendar,
                RemoteId = se.RemoteId,
                RemoteOrigin = se.RemoteOrigin,
                iCalUID = se.iCalUID
            };
        }

        public DeletedSyncedEvent Remove(ulong num)
        {
            DeletedSyncedEvent dse;
            if(_queue.TryRemove(num, out dse))
            {
                return dse;
            }

            return null;
        }

        public static DeleteQueue Instance
        {
            get
            {
                return _instance;
            }
        }
    }
}
