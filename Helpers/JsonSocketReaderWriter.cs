﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Threading;

namespace CalendarSync.Helpers
{
    public class ServerDisconnectedException : Exception
    {
    }

    /*
     * JsonSocketReaderWriter - a simple naive "protocol" for transporting json objects on the wire 
     * (see JsonSocket: https://www.npmjs.com/package/json-socket)
     * 
     * A json object is serialized into a string with the format "<length>#<jsonpayload>"
     * where <length> is the character length of the <jsonpayload>< to follow
     * 
     * Example (WCNotification):
     * 
     * {
     *     "Provider": "Google", 
     *     "ChannelId": "dff09062-4448-4aa8-941c-48356e26b847"
     * }
     * 
     * Would be serialized as: 75#{"Provider": "Google", "ChannelId": "dff09062-4448-4aa8-941c-48356e26b847"}
     * 
     */
    public class JsonSocketReaderWriter : IDisposable
    {
        protected Stream stream;
        protected StringBuilder sb;
        protected int readBufferSize;

        public JsonSocketReaderWriter(Stream s, int ReadBufferSizeHint=128)
        {
            this.readBufferSize = ReadBufferSizeHint;
            this.stream = s;
            sb = new StringBuilder();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (stream != null)
                {
                    stream.Dispose();
                    stream = null;
                }
            }
        }

        public void SendJson(object o)
        {
            string json = JsonConvert.SerializeObject(o);

            var sendbuffer = Encoding.UTF8.GetBytes(string.Format("{0}#{1}", json.Length, json));

            this.stream.Write(sendbuffer, 0, sendbuffer.Length);
        }

        protected async Task<bool> ReadMore(WaitHandle externalWait)
        {
            byte[] buffer = new byte[readBufferSize];

            var read = stream.ReadAsync(buffer, 0, buffer.Length);

            var firstCompleted = await Task.WhenAny(read, externalWait.WaitOneAsync());

            if (firstCompleted == read)
            {
                if (read.Result > 0)
                {
                    sb.Append(Encoding.UTF8.GetChars(buffer, 0, read.Result));
                    return true;
                }
            }

            return false;
        }

        public T ReadJson<T>(WaitHandle externalWait)
        {
            if (sb.Length <= 3)
                if (!ReadMore(externalWait).Result)
                    return default(T);

            string s = sb.ToString();
            int idx = s.IndexOf('#');

            while (idx<0)
            {
                if (!ReadMore(externalWait).Result)
                    return default(T);

                s = sb.ToString();
                idx = s.IndexOf('#');
            }

            int msg_len = int.Parse(s.Substring(0, idx));

            while(s.Length < (idx+msg_len))
            {
                if (!ReadMore(externalWait).Result)
                    return default(T);

                s = sb.ToString();
            }

            sb.Remove(0, idx + msg_len + 1);

            string s_json = s.Substring(idx + 1, msg_len);

            return JsonConvert.DeserializeObject<T>(s_json);
        }
    }
}
