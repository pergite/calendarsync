﻿using System;

using Starcounter;

using CalendarSync.Database;
using OAuth2.Core.Models;
using OAuth2.Core.Services;

namespace CalendarSync.Helpers
{
    public class TokenManager : ITokenManager
    {
        protected OAUser oauser;

        protected TokenManager(OAUser oau)
        {
            oauser = oau;
        }

        public void SetToken(IToken token)
        {
            Db.Transact(() =>
            {
                oauser.Token.AccessToken = token.AccessToken;

                if(null != token.RefreshToken)
                    oauser.Token.RefreshToken = token.RefreshToken;

                oauser.Token.Expires = token.Expires;
            });
        }

        public IToken GetToken()
        {
            return oauser.Token;
        }

        public static TokenManager Get(OAUser oau)
        {
            return new TokenManager(oau);
        }
    }
}
