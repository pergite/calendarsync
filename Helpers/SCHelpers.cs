﻿using System;
using System.Linq;
using System.Collections.Generic;

using Starcounter;

using CalendarSync.Database;

namespace CalendarSync.Helpers
{
    public static class SCHelper
    {
        public static IEnumerable<SyncedCalendar> All()
        {
            return Db.SQL<SyncedCalendar>("SELECT sc FROM CalendarSync.Database.SyncedCalendar sc");
        }

        public static SyncedCalendar Get(string provider, string watchId)
        {
            return Db.SQL<SyncedCalendar>("SELECT sc FROM CalendarSync.Database.SyncedCalendar sc WHERE Provider=? AND WatchId=?", provider, watchId).FirstOrDefault();
        }

        public static IEnumerable<SyncedCalendar> SyncedCalendars(this SystemUser user)
        {
            return Db.SQL<SyncedCalendar>("SELECT sc FROM CalendarSync.Database.SyncedCalendar sc WHERE sc.Owner.SystemUser=?", user);
        }
    }
}
