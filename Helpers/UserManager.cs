﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Starcounter;
using OAuth2.Core.Models;

using CalendarSync.Database;
using CalendarSync.ViewModels;

namespace CalendarSync.Helpers
{
    public class UserManager : IUserManager
    {
        public static UserManager Instance
        {
            get
            {
                return new UserManager();
            }
        }

        public Response ProcessAuthResponse(AuthResponseResult result)
        {
            if(result)
            {
                var oauser = Db.SQL<OAUser>("SELECT ou FROM CalendarSync.Database.OAUser ou WHERE ou.Provider=? AND ou.ProviderId=?", result.User.Provider, result.User.ProviderId).FirstOrDefault();

                Db.Transact(() =>
                {
                    var u = result.User;
                    var t = result.Token;

                    if (null==oauser)
                    {
                        var token = new OAToken()
                        {
                            AccessToken = t.AccessToken,
                            RefreshToken = t.RefreshToken,
                            Expires = t.Expires,
                            Issued = t.Issued
                        };

                        oauser = new OAUser()
                        {
                            Provider = u.Provider,
                            ProviderId = u.ProviderId,
                            Name = u.Name,
                            Email = u.Email,
                            Gender = u.Gender,
                            Locale = u.Locale,
                            Picture = u.Picture,
                            Profile = u.Profile,
                            Token = token,
                            SystemUser = SystemUser.Current
                        };
                    }
                    else
                    {
                        oauser.Token.AccessToken = t.AccessToken;
                        oauser.Token.RefreshToken = t.RefreshToken;
                        oauser.Token.Expires = t.Expires;
                        oauser.Token.Issued = t.Issued;
                    }
                });

                return new RedirectPage
                {
                    RedirectUrl = result.FinalDestination
                };
            }

            return Response.FromStatusCode(401);
        }
    }
}
