﻿using System;
using System.Threading;
using System.Net.Sockets;

using Starcounter;

using CalendarSync.Database;
using CalendarSync.Models;
using CalendarSync.Helpers;
using CalendarSync.Services;
using System.Threading.Tasks;

namespace CalendarSync.Notifications
{
    public class NotificationClient
    {
        protected const string SERVER_ENDPOINT = "eventdispatchor.pergite.com";
        //protected const string SERVER_ENDPOINT = "eventdispatchor.pergite.local.com";
        protected const int SERVER_PORT = 9999;

        protected static NotificationClient client = null;
        protected static object _lock = new object();

        protected bool askedToStop = false;
        protected Thread thread;
        protected ManualResetEvent reset;

        public static void Start()
        {
            if (null == client)
            {
                lock(_lock)
                {
                    if(null==client)
                        client = new NotificationClient();
                }
            }
        }

        // Havent found any event to hook up to in order to detect if our Starcounter app is going down 
        // so the Stop() would currently never be called. Its still left inplace in case such an
        // event suddenly emerges
        public static void Stop()
        {
            if (null == client)
                throw new InvalidOperationException("NotificationClient not running");

            client.askedToStop = true;
            client.reset.Set();
        }

        public static void Reset()
        {
            if (null == client)
                throw new InvalidOperationException("NotificationClient not running");

            client.reset.Set();
        }

        public static void AddWatch(SyncedCalendar sc)
        {
            SyncBase sync = Factory.Get(sc);
            sync.AddWatch(sc);
            
            // reregister
            client.reset.Set();
        }

        public static void RemoveWatch(SyncedCalendar sc)
        {
            if (String.IsNullOrEmpty(sc.WatchId))
                return;

            SyncBase sync = Factory.Get(sc);
            sync.RemoveWatch(sc);

            // reregister
            client.reset.Set();
        }

        public static void RefreshWatch(SyncedCalendar sc)
        {
            if (String.IsNullOrEmpty(sc.WatchId))
                return;

            SyncBase sync = Factory.Get(sc);
            sync.RefreshWatch(sc);

            // reregister
            client.reset.Set();
        }

        public NotificationClient()
        {
            reset = new ManualResetEvent(false);
            thread = new Thread(this.Run);
            thread.Start();
        }

        public void Run()
        {
            Console.WriteLine("NotificationhClient: started!");

            while (!askedToStop)
            {
                Console.WriteLine($"NotificationhClient:  Connecting to {SERVER_ENDPOINT}:{SERVER_PORT}");

                try
                {
                    using (TcpClient client = new TcpClient(SERVER_ENDPOINT, SERVER_PORT))
                    {
                        using (JsonSocketReaderWriter s = new JsonSocketReaderWriter(client.GetStream(), 128)) // readbuffersize=128, enough for one WCNotification
                        {
                            while (true)
                            {
                                // register us!
                                WCRegister register = new WCRegister();

                                Scheduling.RunTask(() =>
                                {
                                    foreach (var syncedCal in Db.SQL<SyncedCalendar>("SELECT sc FROM CalendarSync.Database.SyncedCalendar sc WHERE sc.WatchId IS NOT NULL"))
                                    {
                                        register.Channels.Add(syncedCal.WatchId);
                                    }

                                }).Wait();

                                Console.WriteLine(String.Format("NotificationClient: Registering channels [{0}]", String.Join(",", register.Channels)));

                                s.SendJson(register);

                                reset.Reset();

                                // enter WCNotification loop
                                while (true)
                                {
                                    WCNotification wcn = s.ReadJson<WCNotification>(reset);

                                    if (null == wcn)
                                    {
                                        if (reset.WaitOne(0))
                                        {
                                            // reregister (or quit if askedToStop)
                                            break;
                                        }

                                        // server disconnected us
                                        throw new ServerDisconnectedException();
                                    }
                                    else
                                    {
                                        Console.WriteLine($"NotificationhClient: WCNotification => {wcn.Provider}:{wcn.ChannelId}:{wcn.Message}");

                                        if (wcn.Message == "sync")
                                        {
                                            // TODO?
                                        }
                                        else
                                        {
                                            Scheduling.RunTask(() =>
                                            {
                                                SyncedCalendar sc = SCHelper.Get(wcn.Provider, wcn.ChannelId);

                                                if (null != sc)
                                                {
                                                    SyncBase.QueueIncrSync(sc);
                                                }
                                                else
                                                {
                                                    Console.WriteLine($"NotificationhClient: No SyncedCalendar with Provider:WatchId {wcn.Provider}:{wcn.ChannelId}");
                                                }

                                            });
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (ServerDisconnectedException)
                {
                    Console.WriteLine("NotificationhClient: Server disconnected us! Reconnecting in 2s");
                    Thread.Sleep(2000);
                }
                catch(SocketException)
                {
                    Console.WriteLine($"NotificationhClient: Failed/lost connection to {SERVER_ENDPOINT}! Retrying in 2s");
                    Thread.Sleep(2000);
                }
            }

            Console.WriteLine("NotificationClient: asked to stop, terminating gracefully");
        }
    }
}
