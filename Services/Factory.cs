﻿using System;

using CalendarSync.Helpers;
using CalendarSync.Database;

using OAuth2.Core.Services;

namespace CalendarSync.Services
{
    public class Factory
    {
        public static SyncBase Get(string name, OAUser user)
        {
            if (name.ToLower() == "google")
            {
                var sm = SettingsManager.Instance;
                var tm = TokenManager.Get(user);

                return new Google()
                {
                    Rest = OAuth2.Core.Services.Factory.Get(sm.GetSettings("google"), tm),
                    User = user.SystemUser
                };
            }

            throw new NotImplementedException(String.Format("Calendar service '{0}' not implemented", name));
        }

        public static SyncBase Get(SyncedCalendar sc)
        {
            return Get(sc.Provider, sc.Owner);
        }
    }
}
