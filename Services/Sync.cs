﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Starcounter;
using CalendarSync.Helpers;
using CalendarSync.Database;

namespace CalendarSync.Services
{
    public class AllSyncedCalendars
    {
        public static void FullSync()
        {
            foreach(var sc in SCHelper.All())
            {
                SyncBase.TryFullSync(sc);
            }
        }
    }
}
