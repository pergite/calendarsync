﻿using System;
using System.Linq;
using System.Collections.Generic;

using Starcounter;

using OAuth2.Core.Helpers;

using CalendarSync.Helpers;
using CalendarSync.Models;
using CalendarSync.Database;
using OAuth2.Core.Services;

namespace CalendarSync.Services
{
    public class Google : SyncBase
    {
        protected override void DeleteEvent(DeletedSyncedEvent dse)
        {
            if (null != dse.RemoteId)
            {
                try
                {
                    this.Rest.Delete($"https://www.googleapis.com/calendar/v3/calendars/{dse.Calendar.CalendarId}/events/{dse.RemoteId}");
                }
                catch (RestException rex)
                {
                    // "swallow" if resource has already been deleted (statusCode==410), rethrow otherwise
                    if (rex.StatusCode != 410)
                        throw;
                }
            }
        }

        protected override void SyncEvent(SyncedEvent se)
        {
            DbX.TransactIgnoreHooks(() =>
            {
                var participants = new List<GCAttendeeReference>();

                foreach(var p in se.Participants)
                {
                    participants.Add(new GCAttendeeReference()
                    {
                        email = p.Contact.Email,
                        displayName = p.Contact.Name
                    });
                }

                GCEvent ge = new GCEvent()
                {
                    iCalUID = se.iCalUID,
                    start = new GCDate()
                    {
                        dateTime = DateTime.SpecifyKind(se.BeginTime, DateTimeKind.Utc)
                    },
                    end = new GCDate()
                    {
                        dateTime = DateTime.SpecifyKind(se.EndTime, DateTimeKind.Utc)
                    },
                    summary = se.Name,
                    description = se.Description,
                    location = se.Location,
                    attendees = participants.ToArray()
                };

                string calendarId = se.Calendar.CalendarId;

                /* 
                resource calendars shiuld not allow overlapping bookings so we
                make a final check that the requested slot is still available

                I know, I know - there *is* a slight risk that someone gets "in between"
                with a booking after our check but before our booking, but...

                another (earlier) approach was to instead "invite" the resource calendar to an
                event created on some other "scrap calendar" (below code used the primary calendar for easiness), 
                and then scan through attendees[].responseStatus in the upcoming IncrSync() to see wether 
                it was accepted or not, but it was decided to be too cumbersome to setup as it would require 
                the admin to provide such a "scrap calendar".

                if(se.Calendar.Type == SyncedCalendarTypeEnum.Resource)
                {
                    se.Status = StatusEnum.Tentative;
                    calendarId = "primary";

                    ge.attendees = new GCAttendeeReference[1]
                    {
                        new GCAttendeeReference()
                        {
                            optional = false,
                            comment = "added by calendarsync",
                            email = se.Calendar.CalendarId
                        }
                    };
                }
                */

                if (String.IsNullOrEmpty(se.RemoteId))
                {
                    bool allowInsert = true;

                    if (se.Calendar.Type == SyncedCalendarTypeEnum.Resource)
                    {
                        if (!CheckFree(se.Calendar.CalendarId,ge.start.dateTime, ge.end.dateTime))
                        {
                            allowInsert = false;
                            se.Status = StatusEnum.Declined;
                        }
                    }

                    if (allowInsert)
                    {
                        // insert new event
                        ge = this.Rest.Post<GCEvent>($"https://www.googleapis.com/calendar/v3/calendars/{calendarId}/events", ge);
                        se.RemoteId = ge.id;
                        se.Status = StatusEnum.Confirmed;
                        se.Updated = DateTime.UtcNow;
                    }
                }
                else
                {
                    bool allowUpdate = true;

                    if (se.Calendar.Type == SyncedCalendarTypeEnum.Resource)
                    {
                        if (!se.RemoteOrigin)
                        {
                            // get existing event
                            var exge = this.Rest.Get<GCEvent>($"https://www.googleapis.com/calendar/v3/calendars/{se.Calendar.CalendarId}/events/{se.RemoteId}");

                            DateTime exstart = TimeZoneInfo.ConvertTime(exge.start.dateTime, TimeZoneInfo.Utc);
                            DateTime exend = TimeZoneInfo.ConvertTime(exge.end.dateTime, TimeZoneInfo.Utc);

                            if ((ge.start.dateTime < exstart && ge.end.dateTime <= exstart) ||
                                (ge.start.dateTime >= exend && ge.end.dateTime > exend))
                            {
                                // completely new time not overlapping with old
                                allowUpdate = CheckFree(se.Calendar.CalendarId,ge.start.dateTime, ge.end.dateTime);
                            }
                            else
                            {
                                if ((ge.start.dateTime >= exstart && ge.start.dateTime <= exend) &&
                                    (ge.end.dateTime >= exstart && ge.end.dateTime <= exend))
                                {
                                    // new time is the same or completely "within" the old event
                                    allowUpdate = true;
                                }
                                else
                                {
                                    if(ge.end.dateTime > exend && ge.start.dateTime < exend)
                                    {
                                        // new end time moved later
                                        allowUpdate = CheckFree(se.Calendar.CalendarId,exend, ge.end.dateTime);
                                    }

                                    if (allowUpdate && ge.start.dateTime < exstart && ge.end.dateTime > exstart)
                                    {
                                        // new start time moved earlier
                                        allowUpdate = CheckFree(se.Calendar.CalendarId,ge.start.dateTime, exstart);
                                    }
                                }
                            }
                        }
                        else
                        {
                            allowUpdate = false;
                        }
                    }

                    if(allowUpdate)
                    {
                        // update existing event
                        ge.id = se.RemoteId;
                        ge = this.Rest.Put<GCEvent>($" https://www.googleapis.com/calendar/v3/calendars/{calendarId}/events/{se.RemoteId}", ge);
                        se.Status = StatusEnum.Confirmed;
                        se.Updated = DateTime.UtcNow;
                    }
                    else
                    {
                        se.Status = StatusEnum.Declined;
                    }
                }
            });
        }

        protected override void IncrSync(SyncedCalendar sc)
        {
            if (String.IsNullOrEmpty(sc.SyncToken))
            {
                // We dont have a synctoken, upgrade to fullsync
                Console.WriteLine("No SyncToken for Calendar - Upgrading to FullSync");
                Sync(sc, true);
                return;
            }

            Sync(sc, false);
        }

        protected override void FullSync(SyncedCalendar sc)
        {
            Sync(sc, true);
        }

        protected void Sync(SyncedCalendar sc, bool full)
        {
            string whatsync = (full) ? "Full" : "Incr";

            Dictionary<string, string> parameters = new Dictionary<string, string>()
            {
                { "singleEvents", "true" },
                { "maxResults", "250" },
                { "alt", "json" },
                { "pageToken", null }
            };

            if (full)
            {
                parameters["showDeleted"] = "false";
                parameters["timeMin"] = DateTime.UtcNow.AddYears(-1).ISO8601();
            }
            else
            {
                parameters["showDeleted"] = "true";
                parameters["syncToken"] = sc.SyncToken;
            }

            string nextSyncToken = null;

            try
            {
                HashSet<string> existing = new HashSet<string>();

                DbX.TransactIgnoreHooks(() =>
                {
                    do
                    {
                        var c = this.Rest.Get<GCCalendar>($"https://www.googleapis.com/calendar/v3/calendars/{sc.CalendarId}/events?{RequestParameters.Build(parameters)}");

                        TimeZoneInfo tziUtc = TimeZoneInfo.Utc;

                        foreach (var e in c.items)
                        {
                            var se = Db.SQL<SyncedEvent>("SELECT e FROM CalendarSync.Database.SyncedEvent e WHERE e.Calendar=? AND e.iCalUID=?", sc, e.iCalUID).FirstOrDefault();

                            var now = DateTime.UtcNow;

                            var organizerContact = Db.SQL<Contact>("SELECT c FROM CalendarSync.Database.Contact c WHERE c.Email=?", e.organizer.email).FirstOrDefault();

                            if (null == organizerContact)
                            {
                                organizerContact = new Contact()
                                {
                                    Email = e.organizer.email,
                                    Name = e.organizer.displayName
                                };
                            }

                            DateTime start, end;
                            bool fullDay = false;

                            if(null!=e.start.Date)
                            {
                                fullDay = true;
                                start = DateTime.SpecifyKind(DateTime.ParseExact(e.start.Date, "yyyy-MM-dd", System.Globalization.DateTimeFormatInfo.InvariantInfo), DateTimeKind.Utc);
                                if (null != e.end.Date)
                                    end = DateTime.SpecifyKind(DateTime.ParseExact(e.end.Date, "yyyy-MM-dd", System.Globalization.DateTimeFormatInfo.InvariantInfo), DateTimeKind.Utc);
                                else
                                    end = start.AddDays(1);
                            }
                            else
                            {
                                start = TimeZoneInfo.ConvertTime(e.start.dateTime, tziUtc);
                                end = TimeZoneInfo.ConvertTime(e.end.dateTime, tziUtc);
                            }

                            if (null == se)
                            {
                                if (e.status != "cancelled")
                                {
                                    se = new SyncedEvent()
                                    {
                                        Calendar = sc,
                                        RemoteOrigin = true,
                                        Status = StatusEnum.Confirmed,
                                        RemoteId = e.id,
                                        Created = now,
                                        Updated = now,
                                        iCalUID = e.iCalUID,
                                        FullDay = fullDay,
                                        BeginTime = start,
                                        EndTime = end,
                                        Name = e.summary,
                                        Location = e.location,
                                        Organizer = organizerContact
                                    };
                                }
                            }
                            else
                            {
                                // update or delete
                                if (e.status == "cancelled")
                                {
                                    Db.SQL("DELETE FROM CalendarSync.Database.SyncedEventParticipant WHERE Event=?", se);
                                    se.Delete();
                                }
                                else
                                {
                                    se.Updated = now;
                                    se.FullDay = fullDay;
                                    se.BeginTime = start;
                                    se.EndTime = end;
                                    se.Name = e.summary;
                                    se.Location = e.location;
                                    se.Status = StatusEnum.Confirmed;
                                    se.Organizer = organizerContact;
                                }
                            }

                            if (e.status != "cancelled")
                            {
                                if (null != e.attendees)
                                {
                                    foreach (var p in e.attendees)
                                    {
                                        var participantContact = Db.SQL<Contact>("SELECT c FROM CalendarSync.Database.Contact c WHERE c.Email=?", p.email).FirstOrDefault();

                                        if (null == participantContact)
                                        {
                                            participantContact = new Contact()
                                            {
                                                Email = p.email,
                                                Name = p.displayName
                                            };
                                        }

                                        var syncedEventParticipant = Db.SQL<SyncedEventParticipant>("SELECT sep FROM CalendarSync.Database.SyncedEventParticipant sep WHERE sep.Event=? AND sep.Contact=?", se, participantContact).FirstOrDefault();

                                        if (null == syncedEventParticipant)
                                        {
                                            syncedEventParticipant = new SyncedEventParticipant()
                                            {
                                                Event = se,
                                                Contact = participantContact
                                            };
                                        }

                                        switch (p.responseStatus)
                                        {
                                            case "declined":
                                                syncedEventParticipant.Status = StatusEnum.Declined;
                                                break;

                                            case "accepted":
                                                syncedEventParticipant.Status = StatusEnum.Confirmed;
                                                break;

                                            case "tentative":
                                            default:
                                                syncedEventParticipant.Status = StatusEnum.Tentative;
                                                break;
                                        }
                                    }
                                }
                                existing.Add(e.iCalUID);
                            }
                        }

                        parameters["pageToken"] = c.nextPageToken;
                        nextSyncToken = c.nextSyncToken;

                    } while (null != parameters["pageToken"]);

                    if (full)
                    {
                        foreach (var se in sc.Events)
                        {
                            if (!existing.Contains(se.iCalUID))
                                se.Delete();
                        }
                    }

                    sc.LastSync = DateTime.UtcNow;
                    sc.SyncToken = nextSyncToken;
                });
            }
            catch (RestException rex)
            {
                if (rex.StatusCode == 410)
                {
                    // synctoken is no longer valid - upgrade to fullsync
                    Console.WriteLine("SyncToken is no longer valid - Upgrading to fullsync");
                    Sync(sc, true);
                    return;
                }

                Console.WriteLine("{0}Sync for SyncedCalendar {1} failed with {2} {3}", whatsync, sc.CalendarId, rex.StatusCode, rex.ResponseContent);
            }
            catch (Exception ex)
            {
                Console.WriteLine("{0}Sync for SyncedCalendar {1} failed with", whatsync, sc.CalendarId);
                Console.WriteLine(ex);
            }
        }

        public override void AddWatch(SyncedCalendar sc)
        {
            var watch = new GCWatch()
            {
                id = Guid.NewGuid().ToString("D"),
                type = "web_hook",
                address = "https://eventdispatchor.pergite.com/notifications/google"
            };

            var sreturn = this.Rest.Post<GCWatch, GCWatchResponse>($"https://www.googleapis.com/calendar/v3/calendars/{sc.CalendarId}/events/watch", watch);

            Db.Transact(() =>
            {
                sc.WatchId = watch.id;
                sc.WatchRemoteId = sreturn.resourceId;
                sc.WatchExpires = sreturn.expiration.FromUnixTime();
            });
        }

        public override void RemoveWatch(SyncedCalendar sc)
        {
            if (String.IsNullOrEmpty(sc.WatchId))
                return;

            var stop = new GCWatchStop()
            {
                id = sc.WatchId,
                resourceId = sc.WatchRemoteId
            };

            try
            {
                this.Rest.Post<GCWatchStop, NoContent>($"https://www.googleapis.com/calendar/v3/channels/stop", stop);

                Db.Transact(() =>
                {
                    sc.WatchId = null;
                    sc.WatchRemoteId = null;
                    sc.WatchExpires = DateTime.MinValue;
                });
            }
            catch (RestException rex)
            {
                Console.WriteLine("RemoveWatch failed {0} {1}", rex.StatusCode, rex.ResponseContent);
            }
        }

        public override void RefreshWatch(SyncedCalendar sc)
        {
            if (String.IsNullOrEmpty(sc.WatchId))
                return;

            var stop = new GCWatchStop()
            {
                id = sc.WatchId,
                resourceId = sc.WatchRemoteId
            };

            try
            {
                this.Rest.Post<GCWatchStop, NoContent>($"https://www.googleapis.com/calendar/v3/channels/stop", stop);

                var watch = new GCWatch()
                {
                    id = Guid.NewGuid().ToString("D"),
                    type = "web_hook",
                    address = "https://eventdispatchor.pergite.com/notifications/google"
                };

                var sreturn = this.Rest.Post<GCWatch, GCWatchResponse>($"https://www.googleapis.com/calendar/v3/calendars/{sc.CalendarId}/events/watch", watch);

                Db.Transact(() =>
                {
                    sc.WatchId = watch.id;
                    sc.WatchRemoteId = sreturn.resourceId;
                    sc.WatchExpires = sreturn.expiration.FromUnixTime();
                });
            }
            catch (RestException rex)
            {
                Console.WriteLine("RefreshWatch failed {0} {1}", rex.StatusCode, rex.ResponseContent);
            }
        }

        public bool CheckFree(string calendarId, DateTime start, DateTime end)
        {
            GCFreeBusyRequest frb = new GCFreeBusyRequest()
            {
                timeMin = start,
                timeMax = end,
                calendarExpansionMax = 1,
                items = new GCId[] {
                    new GCId()
                    {
                        id = calendarId
                    }
                }
            };

            var fb = this.Rest.Post<GCFreeBusyRequest, GCFreeBusyResponse>("https://www.googleapis.com/calendar/v3/freeBusy", frb);

            return (0 == fb.calendars[calendarId].busy.Length);
        }
    }
}
