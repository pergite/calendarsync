﻿using System;
using System.Threading;
using System.Collections.Concurrent;

using Starcounter;
using OAuth2.Core.Services;

using CalendarSync.Models;
using CalendarSync.Database;


namespace CalendarSync.Services
{
    public abstract class SyncBase
    {
        public SystemUser User { get; set; }
        public ServiceBase Rest { get; set; }

        public static ConcurrentQueue<SyncedEvent> syncQueue = new ConcurrentQueue<SyncedEvent>();
        public static ConcurrentQueue<DeletedSyncedEvent> delQueue = new ConcurrentQueue<DeletedSyncedEvent>();
        public static ConcurrentDictionary<SyncedCalendar, int> incrSyncQueue = new ConcurrentDictionary<SyncedCalendar, int>();

        public SyncBase()
        {
        }

        public static void QueueSyncEvent(SyncedEvent se)
        {
            syncQueue.Enqueue(se);

            Scheduling.RunTask(() =>
            {
                SyncedEvent qse;
                if(syncQueue.TryDequeue(out qse))
                    Factory.Get(qse.Calendar).SyncEvent(qse);
            });
        }

        public static void QueueDeleteEvent(DeletedSyncedEvent dse)
        {
            delQueue.Enqueue(dse);
            Scheduling.RunTask(() =>
            {
                DeletedSyncedEvent qdse;

                if(delQueue.TryDequeue(out qdse))
                    Factory.Get(qdse.Calendar).DeleteEvent(qdse);

            });
        }

        public static void QueueIncrSync(SyncedCalendar sc)
        {
            var count = incrSyncQueue.AddOrUpdate(sc, 1, (k, i) => i+1);
            if (count > 1)
                return; //already scheduled

            Scheduling.RunTask(() =>
            {
                while (true)
                {
                    Factory.Get(sc).IncrSync(sc);

                    var nc = incrSyncQueue.AddOrUpdate(sc, 1, (k, i) => i-1);
                    if (nc <= 0)
                        break;
                }

            });
        }

        public static void TryFullSync(SyncedCalendar sc)
        {
            Scheduling.RunTask(() =>
            {
                if(Monitor.TryEnter(sc))
                {
                    try
                    {
                        Factory.Get(sc).FullSync(sc);
                    }
                    finally
                    {
                        Monitor.Exit(sc);
                    }
                }

            });
        }

        protected abstract void SyncEvent(SyncedEvent se);
        protected abstract void DeleteEvent(DeletedSyncedEvent dse);
        protected abstract void FullSync(SyncedCalendar sc);
        protected abstract void IncrSync(SyncedCalendar sc);

        public abstract void AddWatch(SyncedCalendar sc);
        public abstract void RemoveWatch(SyncedCalendar sc);
        public abstract void RefreshWatch(SyncedCalendar sc);
    }
}
