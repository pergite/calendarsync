﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Starcounter;
using CalendarSync.Database;
using CalendarSync.Models;
using CalendarSync.Helpers;
using CalendarSync.Services;

namespace CalendarSync.Handlers
{
    public class Hooks
    {
        public static void Register()
        {
            Hook<SyncedEvent>.CommitInsert += (s, o) => {

                if (o.IsDeleted)
                {
                    o.Delete();
                    return;
                }

                o.iCalUID = String.Format("{0}@calendarsync.starcounterapps.com", Guid.NewGuid().ToString("D"));
                SyncBase.QueueSyncEvent(o);
            };

            Hook<SyncedEvent>.CommitUpdate += (s, o) => {

                if (o.IsDeleted)
                {
                    DeleteQueue.Instance.Set(o);

                    var dse = new DeletedSyncedEvent()
                    {
                        Calendar = o.Calendar,
                        RemoteId = o.RemoteId,
                        RemoteOrigin = o.RemoteOrigin,
                        iCalUID = o.iCalUID
                    };

                    o.Delete();
                    SyncBase.QueueDeleteEvent(dse);
                    return;
                }

                SyncBase.QueueSyncEvent(o);
            };

            Hook<SyncedEvent>.BeforeDelete += (s, o) => {

                if (o.Calendar == null)
                    return;

                o.IsDeleted = true;
                DeleteQueue.Instance.Set(o);
            };

            Hook<SyncedEvent>.CommitDelete += (s, onum) => {

                DeletedSyncedEvent dse = DeleteQueue.Instance.Remove(onum);
                if (null != dse)
                    SyncBase.QueueDeleteEvent(dse);
            };
        }
    }
}
