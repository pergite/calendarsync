﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using Starcounter;
using Starcounter.XSON.Advanced;

using CalendarSync.Database;
using CalendarSync.Helpers;
using CalendarSync.ViewModels;

namespace CalendarSync.Handlers
{
    public class MainHandlers
    {
        public static void Register()
        {
            Handle.GET("/calendarsync/admin", () =>
            {
                var user = SystemUser.Current;

                if (null==user || !user.AllowAdminSettings)
                {
                    return Self.GET("/calendarsync/unauthorized?return_uri=" + HttpUtility.UrlEncode("/calendarsync/admin"));
                }

                return Db.Scope(() => {
                    return new AdminPage()
                    {
                        Data = SettingsManager.Instance.Data
                    };
                });
            });

            Handle.GET("/calendarsync/settings", () =>
            {
                SystemUser user = SystemUser.Current;

                if (null == user || !user.AllowSyncCalendar)
                {
                    return Self.GET("/calendarsync/unauthorized?return_uri=" + HttpUtility.UrlEncode("/calendarsync/settings"));
                }

                // check if current user has a linked google login
                if (null != user.GetOAUser("Google"))
                {
                    Session session = Session.Ensure();

                    var page = new SettingsPage()
                    {
                        SelectedCalendarId = "",
                        Data = null
                    };

                    session.SetClientRoot(page);

                    return page;
                }

                return new SigninPage()
                {
                    Scope = HttpUtility.UrlEncode("https://www.googleapis.com/auth/calendar https://www.googleapis.com/auth/admin.directory.resource.calendar"),
                    FinalDestination = HttpUtility.UrlEncode("/calendarsync/settings")
                };
            });

            Handle.GET("/calendarsync/unauthorized?return_uri={?}", (string returnUri) =>
            {
                return new UnauthorizedPage();
            });

            // Blending
            Blender.MapUri("/calendarsync/unauthorized?return_uri={?}", "userform-return", null);
        }
    }
}
