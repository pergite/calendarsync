﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CalendarSync.Database;

namespace CalendarSync.Models
{
    public class DeletedSyncedEvent
    {
        public SyncedCalendar Calendar;
        public bool RemoteOrigin { get; set; }
        public string RemoteId { get; internal set; }
        public string iCalUID { get; internal set; }
    }
}
