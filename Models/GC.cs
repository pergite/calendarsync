﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CalendarSync.Models
{
    /*
    {
        "dateTime": "2017-09-14T18:00:00+02:00",
        "timeZone": "Europe/Stockholm"
    }
    */
    public class GCDate
    {
        [JsonConverter(typeof(IsoDateTimeConverter))]
        public DateTime dateTime;
        public string timeZone;
        public string Date;
    }
    /*
    {
        "method": "email",
        "minutes": 10
    }
    */
    public class GCReminder
    {
        public string method;
        public int minutes;
    }

    /*
    {
        "email": "johano99@gmail.com",
        "displayName": "Johan Olofsson",
        "self": true
    }
    */
    public class GCReference
    {
        public string id;
        public string email;
        public string displayName;
        public bool? self;
    }

    public class GCAttendeeReference : GCReference
    {
        public bool? organizer;
        public bool? resource;
        public bool? optional;
        public string responseStatus;
        public string comment;
        public int? additionalGuests;
    }


    /*
    {
        "useDefault": true
    }
    */
    public class GCReminderSettings
    {
        public bool useDefault;
    }

    /* 
     {
        "kind": "calendar#calendarListEntry",
        "etag": "\"1508968301408000\"",
        "id": "ipl857ba5kjujuhsrnjsj4e7f8@group.calendar.google.com",
        "summary": "Konferensrum Måsen",
        "timeZone": "Europe/Stockholm",
        "colorId": "7",
        "backgroundColor": "#42d692",
        "foregroundColor": "#000000",
        "selected": true,
        "accessRole": "owner",
        "defaultReminders": [
            {
                "method": "email",
                "minutes": 10
            },
            {
                "method": "popup",
                "minutes": 10
            }
        ]
    }
    */

    public class GCCalendar
    {
        public string id;
        public string kind;
        public string etag;
        public string summary;
        public string description;
        public DateTime updated;
        public string timeZone;
        public string accessRole;
        public GCReminder[] defaultReminders;
        public GCEvent[] items;
        public string nextPageToken;
        public string nextSyncToken;
    }


    /*
    {
        "kind": "admin#directory#resources#calendars#CalendarResource",
        "etags": etag,
        "resourceId": string,
        "resourceName": string,
        "resourceType": string,
        "resourceDescription": string,
        "resourceEmail": string
    }
    */
    public class GCResourceCalendar
    {
        public string kind;
        public string etag;
        public string resourceId;
        public string resourceName;
        public string resourceType;
        public string resourceDescription;
        public string resourceEmail;
    }


    /*
    {
        "kind": "calendar#event",
        "etag": "\"3008759596962000\"",
        "id": "cph34cpj6li38b9j6phm6b9kc9gmabb168s64bb5c9h68e1k6sr66opl6g",
        "status": "tentative",
        "htmlLink": "https://www.google.com/calendar/event?eid=Y3BoMzRjcGo2bGkzOGI5ajZwaG02YjlrYzlnbWFiYjE2OHM2NGJiNWM5aDY4ZTFrNnNyNjZvcGw2ZyBqb2hhbm85OUBt",
        "created": "2017-09-02T19:16:38.000Z",
        "updated": "2017-09-02T19:16:38.481Z",
        "summary": "Fmöte Isabel",
        "location": "Rälsen",
        "creator": {
            "email": "johano99@gmail.com",
            "displayName": "Johan Olofsson",
            "self": true
        },
        "organizer": {
            "email": "johano99@gmail.com",
            "displayName": "Johan Olofsson",
            "self": true
        },
        "start": {
            "dateTime": "2017-09-14T18:00:00+02:00",
            "timeZone": "Europe/Stockholm"
        },
        "end": {
            "dateTime": "2017-09-14T19:00:00+02:00",
            "timeZone": "Europe/Stockholm"
        },
        "iCalUID": "cph34cpj6li38b9j6phm6b9kc9gmabb168s64bb5c9h68e1k6sr66opl6g@google.com",
        "sequence": 1,
        "reminders": {
            "useDefault": true
        }
    }
   */
    public class GCEvent
    {
        public string kind;
        public string etag;
        public string id;
        public string iCalUID;
        public string status;
        public string summary;
        public string location;
        public string description;
        public string htmlLink;
        public DateTime? created;
        public DateTime? updated;
        public GCDate start;
        public GCDate end;
        public GCReference creator;
        public GCReference organizer;
        public GCAttendeeReference[] attendees;
        public GCReminderSettings reminders;
    }

    /*
     "kind": "calendar#calendarList",
     "etag": "\"p320edul6s66de0g\"",
     "nextSyncToken": "CIDm-qbhjNcCEhJqb2hhbm85OUBnbWFpbC5jb20=",
     "items": [
     ]
    */
    public class GCCalendarList
    {
        public string kind;
        public string etag;
        public string nextSyncToken;
        public GCCalendar[] items;
    }


    /*
      "kind": "admin#directory#resources#calendars#calendarResourcesList",
      "etag": etag,
      "nextPageToken": string,
      "items": [
        resources.calendars Resource
      ]
    */
    public class GCResourceCalendarList
    {
        public string kind;
        public string etag;
        public string nextPageToken;
        public GCResourceCalendar[] items;
    }

    /*
    {
      "id": "01234567-89ab-cdef-0123456789ab", // Your channel ID.
      "type": "web_hook",
      "address": "https://mydomain.com/notifications", // Your receiving URL.
      ...
      "token": "target=myApp-myCalendarChannelDest", // (Optional) Your channel token.
      "expiration": 1426325213000 // (Optional) Your requested channel expiration time.
    }    
    */
    public class GCWatch
    {
        public string id;
        public string type;
        public string address;
        public string token;
        public long? expiration;
    }

    /*
    {
      "kind": "api#channel",
      "id": "01234567-89ab-cdef-0123456789ab"", // ID you specified for this channel.
      "resourceId": "o3hgv1538sdjfh", // ID of the watched resource.
      "resourceUri": "https://www.googleapis.com/calendar/v3/calendars/my_calendar@gmail.com/events", // Version-specific ID of the watched resource.
      "token": "target=myApp-myCalendarChannelDest", // Present only if one was provided.
      "expiration": 1426325213000, // Actual expiration time as Unix timestamp (in ms), if applicable.
    }    
    */
    public class GCWatchResponse
    {
        public string kind;
        public string id;
        public string resourceId;
        public string resourceUri;
        public string token;
        public long expiration;
    }

    /*
    {
      "id": "4ba78bf0-6a47-11e2-bcfd-0800200c9a66",
      "resourceId": "ret08u3rv24htgh289g"
    }
    */
    public class GCWatchStop
    {
        public string id;
        public string resourceId;
    }


    public class GCId
    {
        public string id;
    }

    /*
    {
      "timeMin": datetime,
      "timeMax": datetime,
      "timeZone": string,
      "groupExpansionMax": integer,
      "calendarExpansionMax": integer,
      "items": [
        {
          "id": string
        }
      ]
    }     
    */
    public class GCFreeBusyRequest
    {
        public DateTime timeMin;
        public DateTime timeMax;
        public string timeZone;
        public int groupExpansionMax;
        public int calendarExpansionMax;
        public GCId[] items;
    }

    /*
    {
      "kind": "calendar#freeBusy",
      "timeMin": datetime,
      "timeMax": datetime,
      "groups": {
        (key): {
          "errors": [
            {
              "domain": string,
              "reason": string
            }
          ],
          "calendars": [
            string
          ]
        }
      },
      "calendars": {
        (key): {
          "errors": [
            {
              "domain": string,
              "reason": string
            }
          ],
          "busy": [
            {
              "start": datetime,
              "end": datetime
            }
          ]
        }
      }
    }
    */
    public class GCFreeBusyError
    {
        public string domain;
        public string reason;
    }

    public class GCFreeBusyBusy
    {
        public DateTime start;
        public DateTime end;
    }

    public class GCFreeBusyGroups
    {
        public GCFreeBusyError[] errors;
        public GCId[] calendars;
    }

    public class GCFreeBusyCalendars
    {
        public GCFreeBusyError[] errors;
        public GCFreeBusyBusy[] busy;
    }

    public class GCFreeBusyResponse
    {
        public string kind;
        public DateTime timeMin;
        public DateTime timeMax;
        public Dictionary<string, GCFreeBusyGroups> groups;
        public Dictionary<string, GCFreeBusyCalendars> calendars;
    }
}
