﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OAuth2.Core.Models;

namespace CalendarSync.Models
{
    public class OASettings : ISettings
    {
        public string DisplayName { get; set; }
        public string ServiceName { get; set; }
        public string ClientID { get; set; }
        public string ClientSecret { get; set; }
    }
}
