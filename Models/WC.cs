﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalendarSync.Models
{
    public class WCRegister
    {
        public string Provider;
        public List<string> Channels;

        public WCRegister()
        {
            Channels = new List<string>();
        }
    }

    public class WCNotification
    {
        public string Provider;
        public string ChannelId;
        public string Message;
        public string SyncToken;
    }
}
